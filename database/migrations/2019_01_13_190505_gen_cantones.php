<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GenCantones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_cantones', function (Blueprint $table) {
            $table->string('canton_id', 4);
            $table->string('provincia_id', 2);
            $table->string('canton_nombre', 50);

            $table->primary('canton_id');
            $table->foreign('provincia_id')->references('provincia_id')->on('gen_provincias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
