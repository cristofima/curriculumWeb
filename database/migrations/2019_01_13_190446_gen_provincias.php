<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GenProvincias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_provincias', function (Blueprint $table) {
            $table->string('provincia_id', 2);
            $table->integer('region_id');
            $table->string('provincia_nombre', 30);

            $table->primary('provincia_id');
            $table->foreign('region_id')->references('region_id')->on('gen_regiones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
