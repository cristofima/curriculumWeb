<!-- main menu-->
<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
      <!-- main menu header-->
      <!-- / main menu header-->
      <!-- main menu content-->
      <div class="main-menu-content menu-accordion">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
          <li class="{{Request::path()=='dashboard'?'active':''}} nav-item"><a href="{{url('/dashboard')}}"><i class="icon-home3"></i><span data-i18n="" class="menu-title">Inicio</span></a>
          </li>
          <li class=" navigation-header"><span data-i18n="nav.category.support">Cuenta</span><i data-toggle="tooltip" data-placement="right" data-original-title="Cuenta" class="icon-ellipsis icon-ellipsis"></i>
          </li>
          <li class="{{Request::path()=='account'?'active':''}} nav-item"><a href="{{url('/account')}}"><i class="icon-sliders"></i><span data-i18n="" class="menu-title">Configuración</span></a>
          </li>
          <li class="{{Request::path()=='personal_data'?'active':''}} nav-item"><a href="{{url('/personal_data')}}"><i class="icon-user-secret"></i><span data-i18n="" class="menu-title">Datos Personales</span></a>
          </li>
          <li class=" navigation-header"><span data-i18n="nav.category.cv">CV</span><i data-toggle="tooltip" data-placement="right" data-original-title="CV" class="icon-ellipsis icon-ellipsis"></i>
          </li>
          <li class=" nav-item"><a href="#"><i class="icon-graduation-cap"></i><span data-i18n="" class="menu-title">Formación Académica</span></a>
            <ul class="menu-content">
              <li class="{{Request::path()=='studies'?'active':''}} nav-item"><a href="{{url('/studies')}}" class="menu-item"><i class="icon-graduation-cap"></i>Estudios Universitarios</a>
              </li>
              <li class="{{Request::path()=='languajes'?'active':''}} nav-item"><a href="{{url('/languajes')}}" class="menu-item"><i class="icon-language"></i>Idiomas</a>
              </li>
            </ul>
          </li>
          <li class="{{Request::path()=='job_experience'?'active':''}} nav-item"><a href="{{url('/job_experience')}}"><i class="icon-institution"></i><span data-i18n="" class="menu-title">Experiencia Laboral</span></a>
          </li>
          <li class="{{Request::path()=='capacitations'?'active':''}} nav-item"><a href="{{url('/capacitations')}}"><i class="icon-paper"></i><span data-i18n="" class="menu-title">Capacitaciones</span></a>
          </li>
          <li class="{{Request::path()=='certifications'?'active':''}} nav-item"><a href="{{url('/certifications')}}"><i class="icon-archive2"></i><span data-i18n="" class="menu-title">Certificaciones</span></a>
          </li>
          <li class="{{Request::path()=='presentations'?'active':''}} nav-item"><a href="{{url('/presentations')}}"><i class="icon-desktop"></i><span data-i18n="" class="menu-title">Ponencias</span></a>
          </li>
          <li class="{{Request::path()=='acknowledgements'?'active':''}} nav-item"><a href="{{url('/acknowledgements')}}"><i class="icon-trophy3"></i><span data-i18n="" class="menu-title">Méritos y Premios</span></a>
          </li>
          <li class="{{Request::path()=='publications'?'active':''}} nav-item"><a href="{{url('/publications')}}"><i class="icon-book2"></i><span data-i18n="" class="menu-title">Publicaciones</span></a>
          </li>
          <li class=" nav-item"><a href="documentation-maps.html"><i class="icon-users3"></i><span data-i18n="" class="menu-title">Referencias</span></a>
          </li>
        </ul>
      </div>
      <!-- /main menu content-->
      <!-- main menu footer-->
      <!-- main menu footer-->
    </div>
    <!-- / main menu-->