<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="robots" content="noindex, follow">
  <meta name="description" content="Crea tu CV de una manera fácil y rápida">
  <meta name="author" content="Cristopher Coronado">

  <title>Easy Curriculum Vitae</title>

  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/ico/favicon.ico') }}">
  <link rel="shortcut icon" type="image/png" href="{{ asset('images/ico/favicon-32.png') }}">

  <!-- Bootstrap core CSS -->
  {!! Minify::stylesheet('/welcome/vendor/bootstrap/css/bootstrap.min.css')->withFullUrl()!!}

  <!-- Custom fonts for this template -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="{{asset('welcome/css/agency.min.css')}}" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">Easy Curriculum Vitae</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menú
        <i class="fa fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#services">Servicios</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about">Nosotros</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#team">Equipo</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact">Contactos</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Header -->
  <header class="masthead">
    <div class="container">
      <div class="intro-text">
        <div class="intro-heading text-uppercase">¡Bienvenido!</div>
        <div class="intro-lead-in">¡Crea tu Curriculum Vitae de una manera fácil y rápida!</div>
        <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">¡Averigua más!</a>
      </div>
    </div>
  </header>

  <!-- Services -->
  <section id="services">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Servicios</h2>
          <h3 class="section-subheading text-muted">Al utilizar Easy Curriculum Vitae puedes descargar tu CV en formato PDF desde cualquier parte</h3>
        </div>
      </div>
      <div class="row text-center">
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fa fa-circle fa-stack-2x text-primary"></i>
            <i class="fa fa-chart-bar fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">Estadísticas</h4>
          <p class="text-muted">Te proporcionamos estadísticas específicas de la información que ingresas, como total de horas de capacitaciones, tiempo de experiencia laboral, entre otras.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fa fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-file-pdf fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">CV en PDF</h4>
          <p class="text-muted">Toda la información que ingreses, la puedes descargar en un archivo en formato PDF.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fa fa-circle fa-stack-2x text-primary"></i>
            <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">Seguridad</h4>
          <p class="text-muted">Tus datos estarán siempre protegidos, porque sólo tú tendrás acceso a ellos.</p>
        </div>
      </div>
    </div>
  </section>

  <!-- About -->
  <section class="bg-light" id="about">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">¡Tenemos lo que necesita!</h2>
          <h3 class="section-subheading text-muted">Easy Curriculum Vitae tiene todo lo que necesita para elaborar su CV. Todos los puntos necesarios que debe tener un CV, con un diseño sencillo, pero muy bien organizado          </h3>
        </div>
      </div>
    </div>
  </section>

  <!-- Team -->
  <section  id="team">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Nuestro equipo</h2>
          <!-- <h3 class="section-subheading text-muted">Contamos con .</h3> -->
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="team-member">
            <img class="mx-auto rounded-circle" src="{{asset('images/team/cristopher.jpg')}}" alt="Cristopher Coronado">
            <h4>Cristopher Coronado</h4>
            <p class="text-muted">Desarrollador de software</p>
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a target="_blank" href="https://twitter.com/cristofima2016">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a target="_blank" href="https://www.facebook.com/cristopher.coronado.7">
                  <i class="fab fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a target="_blank" href="https://www.linkedin.com/in/cristopher-geovanny-coronado-moreira-a2659796/">
                  <i class="fab fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="bg-light">
    <div class="container text-center">
      <h2 class="section-heading text-uppercase">¡Qué esperas para iniciar!</h2>
      <a class="btn btn-primary btn-xl sr-button text-uppercase" href="{{ route('dashboard') }}">Acceder</a>
    </div>
  </section>

  <!-- Contact -->
  <section id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Contáctanos</h2>
          <h3 class="section-subheading text-muted"><font color="white">Para cualquier duda sobre la aplicación, envíenos un correo electrónico y nos pondremos en contacto con usted lo antes posible</font></h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <form id="contactForm" name="sentMessage" novalidate="novalidate">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input class="form-control" id="name" type="text" placeholder="Nombre *" required="required" data-validation-required-message="Por favor, ingrese su nombre.">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" id="email" type="email" placeholder="Email *" required="required" data-validation-required-message="Por favor, ingrese su e-mail.">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" id="phone" type="tel" placeholder="Número telefónico *" required="required" data-validation-required-message="Por favor, ingrese su número telefónico.">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <textarea class="form-control" id="message" placeholder="Mensaje *" required="required" data-validation-required-message="Por favor, ingrese su mensaje."></textarea>
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 text-center">
                <div id="success"></div>
                <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Enviar mensaje</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <span class="copyright">Copyright &copy; Easy Curriculm Vitae 2018</span>
        </div>
        <div class="col-md-4">
          <ul class="list-inline social-buttons">
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-twitter"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-facebook"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-linkedin"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="col-md-4">
          <ul class="list-inline quicklinks">
            <li class="list-inline-item">
              <a href="#">Políticas de privacidad</a>
            </li>
            <li class="list-inline-item">
              <a href="#">Términos de uso</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  {!! Minify::javascript('/welcome/vendor/jquery/jquery.min.js')->withFullUrl() !!}
  {!! Minify::javascript('/welcome/vendor/bootstrap/js/bootstrap.bundle.min.js')->withFullUrl() !!}

  <!-- Plugin JavaScript -->
  {!! Minify::javascript('/welcome/vendor/jquery-easing/jquery.easing.min.js')->withFullUrl() !!}

  <!-- Contact form JavaScript -->
  {!! Minify::javascript('/welcome/js/jqBootstrapValidation.js')->withFullUrl() !!}
  {!! Minify::javascript('/welcome/js/contact_me.js')->withFullUrl() !!}

  <!-- Custom scripts for this template -->
  {!! Minify::javascript('/welcome/js/agency.min.js')->withFullUrl() !!}

</body>

</html>