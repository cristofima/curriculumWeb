<!-- Modal -->
<div class="modal fade text-xs-left" id="large" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Nuevo Recocimiento</h4>
        </div>
        {!! Form::open(['url' => 'acknowledgements']) !!}
            <input type="hidden" name="usuario_id" value="{{Auth::user()->id}}">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="merito_descripcion">Descripción</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" id="merito_descripcion" class="form-control round" placeholder="Título del reconocimiento" name="merito_descripcion" required>
                                    <div class="form-control-position">
                                        <i class="icon-head"></i>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="merito_institucion">Institución</label>
                                <div class="position-relative has-icon-left">
                                    <input type="text" id="merito_institucion" class="form-control round" placeholder="Institución que avala el reconocimiento" name="merito_institucion" required>
                                        <div class="form-control-position">
                                            <i class="icon-head"></i>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="merito_fecha">Fecha</label>
                            <div class="position-relative has-icon-left">
                                <input type="date"  class="form-control round" id="merito_fecha" name="merito_fecha"  required>
                                <div class="form-control-position">
                                    <i class="icon-calendar5"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          </div>
          <div class="modal-footer">
            <input type="reset" class="btn btn-outline-warning btn-lg" data-dismiss="modal" value="Cancelar">
            <input type="submit" class="btn btn-outline-primary btn-lg" value="Guardar">
          </div>
          {!! Form::close() !!}
      </div>
    </div>
  </div>