@extends('layouts.panel')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
          <h2 class="content-header-title"><i class="icon-trophy3"></i> Reconocimientos</h2>
        </div>
        <div class="col-md-6 col-xs-12 mb-1">
            <button type="button" class="btn btn-info btn-min-width mr-1 mb-1" data-toggle="modal" data-target="#large">Añadir</button>
        </div>
        @include('panel.acknowledgements.modal')
      </div>

     @include('includes.messages')

      <div class="content-body">
        <section>
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                        <h4 class="card-title">Reconocimientos</h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                                <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                                            </ul>
                                        </div>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block card-dashboard">
                                    <p>Listado de méritos y galardones</p>
                                </div>
                                <div id="jstree_demo_div">
                                    <ul>
                                        @foreach($meritos as $merito)    
                                            @php
                                                $subMeritosMonths=Auth::user()->acknowledgements()
                                                ->select(DB::raw('EXTRACT(MONTH FROM merito_fecha) as month'))
                                                ->where(DB::raw('EXTRACT(YEAR FROM merito_fecha)'),$merito->year)
                                                ->orderBy('month','DESC')
                                                ->distinct()->get();
                                            @endphp                                
                                            <li>{{$merito->year}}
                                            <ul>
                                                @foreach($subMeritosMonths as $sub) 
                                                    @php
                                                        $subMeritos=Auth::user()->acknowledgements()
                                                        ->where(DB::raw('EXTRACT(YEAR FROM merito_fecha)'),$merito->year)
                                                        ->where(DB::raw('EXTRACT(MONTH FROM merito_fecha)'),$sub->month)
                                                        ->orderBy('merito_fecha','DESC')->get();
                                                    @endphp     
                                                    <li>{{$meses[$sub->month - 1]}}
                                                        <ul>
                                                        @foreach($subMeritos as $sub)
                                                            <li>{{$sub->merito_descripcion}}</li>
                                                        @endforeach
                                                        </ul>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
        </section>
      </div>
</div>
@endsection
@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
    <script>
        $('#jstree_demo_div').jstree();
    </script>
@endpush