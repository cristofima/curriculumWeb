@extends('layouts.panel')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
          <h2 class="content-header-title"><i class="icon-archive2"></i> Publicaciones</h2>
        </div>
        <div class="col-md-6 col-xs-12 mb-1">
            <button type="button" class="btn btn-info btn-min-width mr-1 mb-1" data-toggle="modal" data-target="#large">Añadir</button>
        </div>
        @include('panel.capacitations.modal')
      </div>

    @include('includes.messages')

      <div class="content-body">
        <section>
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                        <h4 class="card-title">Publicaciones</h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                                <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                                            </ul>
                                        </div>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block card-dashboard">
                                    <p>Listado de publicaciones</p>
                                </div>
                                <div class="table-responsive">
                                    <table id="data-table" class="table table-striped table-bordered">
                                            <thead>
                                                    <tr>
                                                        <th>Título</th>
                                                        <th>Tipo</th>
                                                        <th>Medio</th>
                                                        <th>Fecha</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($publicaciones as $pub)
                                                        <tr>
                                                            <td>{{$pub->publicacion_nombre}}</td>
                                                            <td>{{$pub->getTypePublication()}}</td>
                                                            <td>{{$pub->publicacion_medio}}</td>
                                                            <td>{{$pub->publicacion_fecha}}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                    </table>           
                                                          
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
        </section>
      </div>
</div>
@endsection
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('data_tables/datatables.min.css')}}"/>
@endpush
@push('scripts')
    <script type="text/javascript" src="{{asset('data_tables/datatables.min.js')}}"></script>
    <script>
    $('#data-table').DataTable( {
        responsive: true,
        // select: true,
        // dom: 'B<"clear">lfrtip',
        // buttons: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        }
    } );
    </script>
@endpush