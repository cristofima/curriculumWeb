@extends('layouts.panel')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
          <h2 class="content-header-title"><i class="icon-sliders"></i> Configuración de la Cuenta</h2>
        </div>
      </div>

        @include('includes.messages')

      <div class="content-body">
        <section>
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                        <h4 class="card-title"><i class="icon-user4"></i> Perfil</h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                                <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                                            </ul>
                                        </div>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block">
                                    <form class="form" method="POST" action="{{url('/user_profile')}}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{Auth::user()->id}}">
                                            <div class="form-body">
                                                    <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="col-md-12 text-center">
                                                            <div class="kv-avatar">
                                                                <div class="file-loading">
                                                                    <input id="avatar-2" name="avatar-2" type="file" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Email</label>
                                                                    <div class="position-relative has-icon-left">
                                                                    <input type="text"  class="form-control round" value="{{$usuario->email}}" placeholder="Email para iniciar sesión"  name="email" required>
                                                                            <div class="form-control-position">
                                                                                <i class="icon-envelope"></i>
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="projectinput2">Nombre de usuario</label>
                                                                    <div class="position-relative has-icon-left">
                                                                            <input type="text"  class="form-control round" value="{{$usuario->name}}" placeholder="Nombre de usuario"  name="name" required>
                                                                            <div class="form-control-position">
                                                                                <i class="icon-user4"></i>
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                            
                                                        </div>                                        
                                                    </div>
                                                
                                                <div class="form-actions">
                                                        <button type="reset" class="btn btn-warning mr-1">
                                                            <i class="icon-cross2"></i> Resetear
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">
                                                            <i class="icon-check2"></i> Guardar
                                                        </button>
                                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
        </section>

        <section>
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                        <h4 class="card-title"><i class="icon-lock2"></i> Contraseña</h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                                <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                                            </ul>
                                        </div>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block">
                                    <form class="form" method="POST" action="{{url('/user_password')}}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{Auth::user()->id}}">
                                            <div class="form-body">
                                                    <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Contraseña actual</label>
                                                                    <div class="position-relative has-icon-left">
                                                                    <input type="password"  class="form-control round"  placeholder="Contraseña actual"  name="current_password" required>
                                                                            <div class="form-control-position">
                                                                                <i class="icon-key3"></i>
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="projectinput2">Nueva contraseña</label>
                                                                    <div class="position-relative has-icon-left">
                                                                            <input type="password"  class="form-control round" placeholder="Nueva contraseña"  name="new_password" required>
                                                                            <div class="form-control-position">
                                                                                <i class="icon-key3"></i>
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="projectinput2">Confirmar nueva contraseña</label>
                                                                    <div class="position-relative has-icon-left">
                                                                            <input type="password"  class="form-control round" placeholder="Confirmar nueva contraseña"  name="new_password_confirmation" required>
                                                                            <div class="form-control-position">
                                                                                <i class="icon-key3"></i>
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                        </div>                                        
                                                    </div>
                                                
                                                <div class="form-actions">
                                                        <button type="reset" class="btn btn-warning mr-1">
                                                            <i class="icon-cross2"></i> Resetear
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">
                                                            <i class="icon-check2"></i> Guardar
                                                        </button>
                                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
        </section>
      </div>
</div>
@endsection
@push('styles')
<!-- font icons-->
<link rel="stylesheet" type="text/css" href="{{ asset('fonts/glyphicons.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/css/fileinput.min.css" />
<style>
.kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar {
    display: inline-block;
}
.kv-avatar .file-input {
    display: table-cell;
    width: 213px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@endpush
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/locales/es.js"></script>
<script src="{{asset('js/vue.js')}}"></script>
<script>
$("#avatar-2").fileinput({
    overwriteInitial: true,
    maxFileSize: 1500,
    showClose: false,
    showCaption: false,
    showBrowse: false,
    browseOnZoneClick: true,
    removeLabel: '',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancelar o revertir cambios',
    elErrorContainer: '#kv-avatar-errors-2',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="{{asset("images/default_avatar_male.jpg")}}" alt="Tu avatar"><h6 class="text-muted">Clic para seleccionar</h6>',
    layoutTemplates: {main2: '{preview} {remove} {browse}'},
    allowedFileExtensions: ["jpg", "png", "gif"]
});
</script>     
@endpush