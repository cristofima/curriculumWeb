<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Curriculum Vitae</title>
        <style type="text/css">
            .cv-title{
                color: red;
            }

            .cv-section{
                color: navy;
            }

            .data-image img{
                float:right;
                width:150px;
                height:150px;
            }

            /* estilos para el footer y el numero de pagina */

            h1,h2,h3,h4,h5,h6,p{
                font-family: "Times New Roman";
            }

            @page { 
                margin-left: 75px;
                margin-right: 75px;
            }
            #header {
                position: fixed;
                left: 0px;
                top: -170px;
                right: 0px;
                height: 90px;
                background-color: #FFFFFF;
                color: #2E2E2E;
                text-align: center;
            }
            #footer {
                position: fixed;
                left: 0px;
                bottom: -180px;
                right: 0px;
                height: 50px;
                background-color: #FFFFFF;
                color: #2E2E2E;
            }
            #footer2 {
                position: fixed;
                left: 0px;
                bottom: -130px;
                right: 0px;
                height: 5px;
                background-color: #A9D0F5;
                color: #2E2E2E;
            }
            #footer .page:after {
                content: counter(page, decimal);
                float:right;
                color: #848484;
            }
            .page-break {
                page-break-after: always;
            }
            img.alineadoTextoImagenCentro{
                float:left;
                width: 108px;
                height: 83px;
            }
            .texto {
                color: #848484;
            }
            table {
                        font-size: 12px;
                        margin-right:50px;
                        width: 700px; 
                        text-align: center;
                        border-collapse: collapse; 
                        }
            th {     font-size: 13px;     font-weight: bold;     padding: 8px;
                    
                     }
            td {    padding: 8px;
             }
        </style>
    </head>
    <body>
        <center><h1 class="cv-title">CURRICULUM VITAE</h1></center>
                <h3 class="cv-section">DATOS PERSONALES</h3>
                <hr/>
                <div class="data-image">
                    @if (Auth::user()->personalData==null)
                          <img src="{{asset('images/portrait/small/avatar-s-1.png')}}" alt="avatar"><i></i>
                        @else
                          @if (Auth::user()->personalData->dato_foto==null)
                            <img src="{{asset('images/portrait/small/avatar-s-1.png')}}" alt="avatar"><i></i>
                          @else
                          <img src="data:image/jpeg;base64,{{stream_get_contents(Auth::user()->personalData->dato_foto)}}" alt="avatar"><i></i>
                          @endif
                    @endif
                </div>
                <div>
                    <p><b>Nombre Completo: </b>{{$personalData->dato_nombres}} {{$personalData->dato_apellidos}}</p>
                    <p><b>Cédula de identidad: </b>{{$personalData->dato_cedula}}</p>
                    <p><b>Fecha de nacimiento: </b>{{$personalData->dato_fecha_nacimiento}}</p>
                    <p><b>Celular: </b>{{$personalData->dato_celular}}</p>
                    <p><b>E-mail: </b>{{$personalData->dato_email}}</p>
                </div>

        <h3 class="cv-section">FORMACIÓN ACADÉMICA</h3>
        <hr/>
        <h4>Estudios Universitarios</h4>
        <table border="1">
            <thead>
                    <tr>
                        <th>Institución</th>
                        <th>Carrera</th>
                        <th>Estado</th>
                        <th>Ciudad</th>
                        <th>Periodo</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($estudios as $est)
                        <tr>
                            <td>{{$est->estudio_institucion}}</td>
                            <td>{{$est->estudio_carrera}}</td>
                            <td>
                                @if($est->estudio_vigente)
                                    {{$est->estudio_semestre}} {{'Semestre'}}
                                @else
                                    {{'TERMINADO'}}
                                @endif
                            </td>
                            <td>
                                @if($est->estudio_extranjero == null)
                                    {{$est->canton->canton_nombre}}, {{$est->canton->province->provincia_nombre}}
                                @else
                                    {{$est->estudio_extranjero}}
                                @endif
                            </td>
                            <td>
                                @if($est->estudio_fin == null)
                                    {{$est->estudio_inicio}} {{'al presente'}}
                                @else
                                    {{$est->estudio_inicio}} / {{$est->estudio_fin}}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
        </table>

        <h3 class="cv-section">EXPERIENCIA LABORAL</h3>

        <h3 class="cv-section">CURSOS & TALLERES</h3>

        @php
            $suma_horas_cursos=$cursos->sum('capacitacion_horas');
            $suma_horas_talleres=$talleres->sum('capacitacion_horas');
            $suma_horas_conferencias=$conferencias->sum('capacitacion_horas');
        @endphp
        <h4>Cursos</h4>
        <table border="1">
        <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Cargo</th>
                    <th>Institución</th>
                    <th>Año</th>
                    <th>Horas</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cursos as $cap)
                    <tr>
                        <td>{{$cap->capacitacion_nombre}}</td>
                        <td>
                            @php   
                                $cargo=$cap->capacitacion_cargo; 
                                if($cargo=='OR'){
                                    echo 'Organizador';
                                }else if($cargo=='AS'){
                                    echo 'Asistente';
                                }else if($cargo=='VO'){
                                    echo 'Voluntario';
                                }else if($cargo=='FA'){
                                    echo 'Facilitador';
                                }
                            @endphp
                        </td>
                        <td>{{$cap->capacitacion_institucion}}</td>
                        <td>{{$cap->getYearCapacitation()}}</td>
                        <td style="text-align:right">{{$cap->capacitacion_horas}}</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td style="text-align:right;font-weight: bold;" colspan="4">TOTAL</td>
                    <td style="text-align:right;font-weight: bold;">{{$suma_horas_cursos}}</td>
                </tr>
            </tfoot>
</table>  

        <h4>Talleres</h4>
        <table border="1">
        <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Cargo</th>
                    <th>Institución</th>
                    <th>Año</th>
                    <th>Horas</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($talleres as $cap)
                    <tr>
                        <td>{{$cap->capacitacion_nombre}}</td>
                        <td>
                            @php   
                                $cargo=$cap->capacitacion_cargo; 
                                if($cargo=='OR'){
                                    echo 'Organizador';
                                }else if($cargo=='AS'){
                                    echo 'Asistente';
                                }else if($cargo=='VO'){
                                    echo 'Voluntario';
                                }else if($cargo=='FA'){
                                    echo 'Facilitador';
                                }
                            @endphp
                        </td>
                        <td>{{$cap->capacitacion_institucion}}</td>
                        <td>{{$cap->getYearCapacitation()}}</td>
                        <td style="text-align:right">{{$cap->capacitacion_horas}}</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td style="text-align:right;font-weight: bold;" colspan="4">TOTAL</td>
                    <td style="text-align:right;font-weight: bold;">{{$suma_horas_talleres}}</td>
                </tr>
            </tfoot>
</table> 

        <h3 class="cv-section">SEMINARIOS, CONGRESOS & JORNADAS</h3>
        <hr/>

<table border="1">
        <thead>
                <tr>
                    <th>Evento</th>
                    <th>Cargo</th>
                    <th>Institución</th>
                    <th>Año</th>
                    <th>Horas</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($conferencias as $cap)
                    <tr>
                        <td>{{$cap->capacitacion_nombre}}</td>
                        <td>
                            @php   
                                $cargo=$cap->capacitacion_cargo; 
                                if($cargo=='OR'){
                                    echo 'Organizador';
                                }else if($cargo=='AS'){
                                    echo 'Asistente';
                                }else if($cargo=='VO'){
                                    echo 'Voluntario';
                                }else if($cargo=='FA'){
                                    echo 'Facilitador';
                                }
                            @endphp
                        </td>
                        <td>{{$cap->capacitacion_institucion}}</td>
                        <td>{{$cap->getYearCapacitation()}}</td>
                        <td style="text-align:right">{{$cap->capacitacion_horas}}</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td style="text-align:right;font-weight: bold;" colspan="4">TOTAL</td>
                    <td style="text-align:right;font-weight: bold;">{{$suma_horas_conferencias}}</td>
                </tr>
            </tfoot>
        </table>

        <h3 class="cv-section">PONENCIAS</h3>
        <hr/>

        <table border="1">
        <thead>
                <tr>
                    <th>Tema</th>
                    <th>Evento</th>
                    <th>Institución</th>
                    <th>Ciudad</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($ponencias as $po)
                    <tr>
                        <td>{{$po->ponencia_tema}}</td>
                        <td>{{$po->ponencia_evento}}</td>
                        <td>{{$po->ponencia_institucion}}</td>
                        <td>{{$po->ponencia_ubicacion}}</td>
                        <td>{{$po->ponencia_fecha}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <h3 class="cv-section">PERFIL PROFESIONAL</h3>
        <hr/>
        <p>{{$personalData->dato_perfil}}</p>

        <!--footer para cada pagina-->
        <div id="footer">
            <!--aqui se muestra el numero de la pagina en numeros romanos-->
            <p class="page"><small class="texto">Curriculum Vitae</small></p>
        </div>
        <div id="footer2">
            <!--aqui se muestra el numero de la pagina en numeros romanos-->
        </div>
        <!--<div class="page-break"></div>-->
    </body>
</html>