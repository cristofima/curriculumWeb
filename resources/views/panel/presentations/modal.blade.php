<!-- Modal -->
<div class="modal fade text-xs-left" id="large" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Nueva Ponencia</h4>
        </div>
        <form method="POST" action="{{url('/capacitations')}}">
                {{ csrf_field() }}
            <input type="hidden" name="usuario_id" value="{{Auth::user()->id}}">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="capacitacion_nombre">Tema</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" id="capacitacion_nombre" class="form-control round" placeholder="Título de capacitación" name="capacitacion_nombre" required>
                                    <div class="form-control-position">
                                        <i class="icon-head"></i>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="capacitacion_institucion">Evento</label>
                                <div class="position-relative has-icon-left">
                                    <input type="text" id="capacitacion_institucion" class="form-control round" placeholder="Institución donde se dió la capacitación" name="capacitacion_institucion" required>
                                        <div class="form-control-position">
                                            <i class="icon-head"></i>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="ponencia_institucion">Institución</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" id="ponencia_institucion" class="form-control round" placeholder="Institución donde se dió la capacitación" name="ponencia_institucion" required>
                                <div class="form-control-position">
                                   <i class="icon-head"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="capacitacion_inicio">Fecha</label>
                            <div class="position-relative has-icon-left">
                                <input type="date"  class="form-control round" id="capacitacion_inicio" name="capacitacion_inicio"  required>
                                <div class="form-control-position">
                                    <i class="icon-calendar5"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          </div>
          <div class="modal-footer">
            <input type="reset" class="btn btn-outline-warning btn-lg" data-dismiss="modal" value="Cancelar">
            <input type="submit" class="btn btn-outline-primary btn-lg" value="Guardar">
          </div>
        </form>
      </div>
    </div>
  </div>