<!-- Modal -->
<div class="modal fade text-xs-left" id="modal-idioma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" id="modal-content-create">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Nuevo Idioma</h4>
        </div>
        <form class="form" method="POST" action="{{url('/languajes')}}">
                {{ csrf_field() }}
            <input type="hidden" name="usuario_id" value="{{Auth::user()->id}}">
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="select_idioma">Idioma</label>
                                <select style="width: 100%" id="select_idioma" name="idioma_id" class="form-control round" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Idioma" required>
                                    @foreach($idiomas as $id)
                                        <option value="{{$id->idioma_id}}">{{$id->idioma_nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                <div class="row">
                        <div class="col-md-4">
                                <div class="form-group">
                                    <label for="idiomaext_nivel_leido">Nivel Leído</label>
                                    <div class="position-relative has-icon-left">
                                            <select id="idiomaext_nivel_leido" name="idiomaext_nivel_leido" class="form-control round" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Nivel Leído" required>
                                                <option value="B">Básico</option>
                                                <option value="I">Intermedio</option>
                                                <option value="A">Avanzado</option>
                                                <option value="N">Nativo</option>
                                            </select>
                                                <div class="form-control-position">
                                            <i class="icon-head"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="idiomaext_nivel_hablado">Nivel Hablado</label>
                                    <div class="position-relative has-icon-left">
                                            <select id="idiomaext_nivel_hablado" name="idiomaext_nivel_hablado" class="form-control round" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Nivel Hablado" required>
                                                <option value="B">Básico</option>
                                                <option value="I">Intermedio</option>
                                                <option value="A">Avanzado</option>
                                                <option value="N">Nativo</option>
                                            </select>
                                                <div class="form-control-position">
                                            <i class="icon-head"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="idiomaext_nivel_escrito">Nivel Escrito</label>
                                    <div class="position-relative has-icon-left">
                                            <select id="idiomaext_nivel_escrito" name="idiomaext_nivel_escrito" class="form-control round" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Nivel Escrito" required>
                                                <option value="B">Básico</option>
                                                <option value="I">Intermedio</option>
                                                <option value="A">Avanzado</option>
                                                <option value="N">Nativo</option>
                                            </select>
                                                <div class="form-control-position">
                                            <i class="icon-head"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                </div>
          </div>
          <div class="modal-footer">
            <input type="reset" class="btn btn-outline-warning btn-lg" data-dismiss="modal" value="Cancelar">
            <input type="submit" class="btn btn-outline-primary btn-lg" value="Guardar">
          </div>
        </form>
      </div>
    </div>
  </div>