@extends('layouts.panel')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
          <h2 class="content-header-title"><i class="icon-language"></i> Idiomas</h2>
        </div>
        <div class="col-md-6 col-xs-12 mb-1">
            <button type="button" class="btn btn-info btn-min-width mr-1 mb-1" data-toggle="modal" data-target="#modal-idioma">Añadir</button>
        </div>
      </div>

      @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade in mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ $message }}
        </div>
    @endif

    @if (count($errors)>0)
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ $error }}
            </div>
        @endforeach
    @endif

      <div class="content-body">
        <section>
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                        <h4 class="card-title">Idiomas</h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                                <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                                            </ul>
                                        </div>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block card-dashboard">
                                    <p>Listado de idiomas</p>
                                </div>
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                                <tr>
                                                    <th>Idioma</th>
                                                    <th>Escrito</th>
                                                    <th>Hablado</th>
                                                    <th>Leído</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($idiomasExt as $id)
                                                    <tr>
                                                        <td>{{$id->languaje->idioma_nombre}}</td>
                                                        <td>{{$id->getName($id->idiomaext_nivel_escrito)}}</td>
                                                        <td>{{$id->getName($id->idiomaext_nivel_hablado)}}</td>
                                                        <td>{{$id->getName($id->idiomaext_nivel_leido)}}</td>
                                                        <td>
                                                            <button type="button" title="Editar"  class="btn btn-success"><i class="icon-pencil-square-o"></i></button>
                                                            <button type="button" title="Eliminar" class="btn btn-danger"><i class="icon-trash2"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                            @if(count($idiomasExt) == 0)
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="4">No existen registros</td>
                                                    </tr>
                                                </tfoot>
                                            @endif
                                </table>           
                                {{$idiomasExt->render()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
        </section>
      </div>
</div>
@include('panel.languajes.modal')
@endsection