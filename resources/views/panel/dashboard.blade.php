@extends('layouts.panel')

@section('content')
<div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- Statistics -->
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="media">
                                <div class="p-2 text-xs-center bg-blue-grey bg-darken-2 media-left media-middle">
                                    <i class="icon-graduation-cap font-large-2 white"></i>
                                </div>
                                <div class="p-2 bg-blue-grey white media-body">
                                    <h5>Títulos</h5>
                                    <h5 class="text-bold-400">{{$estudios}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="media">
                                <div class="p-2 text-xs-center bg-purple bg-darken-2 media-left media-middle">
                                    <i class="icon-language font-large-2 white"></i>
                                </div>
                                <div class="p-2 bg-purple white media-body">
                                    <h5>Idiomas</h5>
                                    <h5 class="text-bold-400">{{$lenguajes}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="media">
                                <div class="p-2 text-xs-center bg-cyan bg-darken-2 media-left media-middle">
                                    <i class="icon-paper font-large-2 white"></i>
                                </div>
                                <div class="p-2 bg-cyan white media-body">
                                    <h5>Capacitaciones</h5>
                                    <h5 class="text-bold-400">{{$capacitaciones_horas}} horas</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="media">
                                <div class="p-2 text-xs-center bg-deep-orange bg-darken-2 media-left media-middle">
                                    <i class="icon-institution font-large-2 white"></i>
                                </div>
                                <div class="p-2 bg-deep-orange white media-body">
                                    <h5>Experiencia</h5>
                                    <h5 class="text-bold-400">{{$tiempo_experiencia}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="media">
                                <div class="p-2 text-xs-center bg-teal bg-darken-2 media-left media-middle">
                                    <i class="icon-desktop font-large-2 white"></i>
                                </div>
                                <div class="p-2 bg-teal white media-body">
                                    <h5>Ponencias</h5>
                                    <h5 class="text-bold-400">{{$ponencias}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="media">
                                <div class="p-2 text-xs-center bg-pink bg-darken-2 media-left media-middle">
                                    <i class="icon-trophy3 font-large-2 white"></i>
                                </div>
                                <div class="p-2 bg-pink white media-body">
                                    <h5>Reconocimientos</h5>
                                    <h5 class="text-bold-400">{{$meritos}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="media">
                                <div class="p-2 text-xs-center bg-amber bg-darken-2 media-left media-middle">
                                    <i class="icon-book2 font-large-2 white"></i>
                                </div>
                                <div class="p-2 bg-amber white media-body">
                                    <h5>Publicaciones</h5>
                                    <h5 class="text-bold-400">{{$publicaciones}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="media">
                                <div class="p-2 text-xs-center bg-blue bg-darken-2 media-left media-middle">
                                    <i class="icon-archive2 font-large-2 white"></i>
                                </div>
                                <div class="p-2 bg-blue white media-body">
                                    <h5>Certificaciones</h5>
                                    <h5 class="text-bold-400">{{$certificaciones}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Capacitaciones Horas por Año</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                    <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                    <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body collapse in">
                            <div class="card-block">
                                <canvas id="column-chart" height="400"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Top 10 capacitaciones más extensas</h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                    <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                    <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="table-responsive">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th>Nro</th>
                                <th>Título</th>
                                <th>Tipo</th>
                                <th>Institución</th>
                                <th>Año</th>
                                <th>Horas</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $con=1; @endphp
                            @foreach($mejores_capacitaciones as $cap)
                                <tr>
                                    <td>{{$con}}</td>                                
                                    <td>{{$cap->capacitacion_nombre}}</td>
                                    <td>{{$cap->getTypeCapacitation()}}</td>
                                    <td>{{$cap->capacitacion_institucion}}</td>
                                    <td>{{$cap->getYearCapacitation()}}</td>
                                    <td>{{$cap->capacitacion_horas}}</td>
                                    @php $con++; @endphp
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
@endsection
@push('scripts')
<script src="{{asset('vendors/js/charts/chart.min.js')}}" type="text/javascript"></script>
<script>
        var url = "{{url('capacitations/chart')}}";
        var Years = new Array();
        var Cursos = new Array();
        var Talleres = new Array();
        var Seminarios = new Array();
        var Congresos = new Array();
        var Jornadas = new Array();
        $(document).ready(function(){
          $.get(url, function(response){
            var tamAnt=0;
            var tamAct=0;
            response.forEach(function(data){
                tamAnt=Years.length;
                if($.inArray(data.year, Years) === -1){
                    Years.push(data.year);
                }
                tamAct=Years.length;
                Cursos=checkValues(Cursos,tamAnt,tamAct);
                Talleres=checkValues(Talleres,tamAnt,tamAct);
                Seminarios=checkValues(Talleres,tamAnt,tamAct);
                Congresos=checkValues(Congresos,tamAnt,tamAct);
                Jornadas=checkValues(Jornadas,tamAnt,tamAct);

                Cursos=checkType(Cursos,data.capacitacion_tipo,'CU',data.hours);
                Talleres=checkType(Talleres,data.capacitacion_tipo,'TA',data.hours);
                Seminarios=checkType(Seminarios,data.capacitacion_tipo,'SE',data.hours);
                Congresos=checkType(Congresos,data.capacitacion_tipo,'CO',data.hours);
                Jornadas=checkType(Jornadas,data.capacitacion_tipo,'JO',data.hours);
            });
            var ctx = document.getElementById("column-chart").getContext('2d');
                var myChart = new Chart(ctx, {
                  type: 'bar',
                  data: {
                      labels:Years,
                      datasets: [
                        {
                            label: 'Cursos',
                            data: Cursos,
                            backgroundColor: "#673AB7",
                            borderColor: "transparent"
                        },
                        {
                            label: 'Talleres',
                            data: Talleres,
                            backgroundColor: "#E91E63",
                            borderColor: "transparent"
                        },
                        {
                            label: 'Seminarios',
                            data: Seminarios,
                            backgroundColor: "#FAAF00",
                            borderColor: "transparent"
                        },
                        {
                            label: 'Congresos',
                            data: Congresos,
                            backgroundColor: "#9896ff",
                            borderColor: "transparent"
                        },
                        {
                            label: 'Jornadas',
                            data: Jornadas,
                            backgroundColor: "#3fcda9",
                            borderColor: "transparent"
                        }
                    ]
                  },
                   // Chart Options
                    options : chartOptions,
              });
          });

          // Chart Options
        var chartOptions = {
            // Elements options apply to all of the options unless overridden in a dataset
            // In this case, we are setting the border of each bar to be 2px wide and green
            elements: {
                rectangle: {
                    borderWidth: 2,
                    borderColor: 'rgb(0, 255, 0)',
                    borderSkipped: 'bottom'
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            responsiveAnimationDuration:500,
            legend: {
                position: 'top',
            },
            scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        color: "#f3f3f3",
                        drawTicks: false,
                    },
                    scaleLabel: {
                        display: true,
                    }
                }],
                yAxes: [{
                    display: true,
                    gridLines: {
                        color: "#f3f3f3",
                        drawTicks: false,
                    },
                    scaleLabel: {
                        display: true,
                    }
                }]
            },
            title: {
                display: true,
                text: 'Capacitaciones Horas por Año'
            }
        };

        function checkValues(array,tamAnt,tamAct){
            if(array.length < tamAnt && tamAct > tamAnt){
                array.push(0);
            }
            return array;
        }

        function checkType(array,value,type,data){
            if(value == type){
                array.push(data);
            }
            return array;
        }

        });
        </script>
@endpush