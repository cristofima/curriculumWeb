<!-- Modal -->
<div class="modal fade text-xs-left" id="large" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Nueva Capacitación</h4>
        </div>
        <form method="POST" action="{{url('/capacitations')}}">
                {{ csrf_field() }}
            <input type="hidden" name="usuario_id" value="{{Auth::user()->id}}">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="capacitacion_nombre">Título</label>
                            <div class="position-relative has-icon-left">
                                <input type="text" id="capacitacion_nombre" class="form-control round" placeholder="Título de capacitación" name="capacitacion_nombre" required>
                                    <div class="form-control-position">
                                        <i class="icon-head"></i>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="capacitacion_institucion">Institución</label>
                                <div class="position-relative has-icon-left">
                                    <input type="text" id="capacitacion_institucion" class="form-control round" placeholder="Institución donde se dió la capacitación" name="capacitacion_institucion" required>
                                        <div class="form-control-position">
                                            <i class="icon-head"></i>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="row">
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label for="capacitacion_tipo">Tipo</label>
                                    <div class="position-relative has-icon-left">
                                            <select id="capacitacion_tipo" name="capacitacion_tipo" class="form-control round" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Tipo" required>
                                                <option value="CU">Curso</option>
                                                <option value="TA">Taller</option>
                                                <option value="SE">Seminario</option>
                                                <option value="CO">Congreso</option>
                                                <option value="JO">Jornada</option>
                                            </select>
                                                <div class="form-control-position">
                                            <i class="icon-head"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="capacitacion_cargo">Cargo</label>
                            <div class="position-relative has-icon-left">
                                    <select id="capacitacion_cargo" name="capacitacion_cargo" class="form-control round" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Cargo" required>
                                        <option value="OR">Organizador</option>
                                        <option value="AS">Asistente</option>
                                        <option value="FA">Facilitador</option>
                                        <option value="VO">Voluntario</option>
                                    </select>
                                        <div class="form-control-position">
                                    <i class="icon-head"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="capacitacion_inicio">Fecha de inicio</label>
                            <div class="position-relative has-icon-left">
                                <input type="date"  class="form-control round" id="capacitacion_inicio" name="capacitacion_inicio"  required>
                                <div class="form-control-position">
                                    <i class="icon-calendar5"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="capacitacion_fin">Fecha de fin</label>
                            <div class="position-relative has-icon-left">
                                <input type="date"  class="form-control round" id="capacitacion_fin" name="capacitacion_fin">
                                <div class="form-control-position">
                                    <i class="icon-calendar5"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                            <div class="form-group">
                                <label for="capacitacion_horas">Horas</label>
                                <div class="position-relative has-icon-left">
                                    <input type="number" class="form-control round" placeholder="Horas de capacitación" name="capacitacion_horas" id="capacitacion_horas" min="1" required>
                                    <div class="form-control-position">
                                        <i class="icon-calendar5"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
          </div>
          <div class="modal-footer">
            <input type="reset" class="btn btn-outline-warning btn-lg" data-dismiss="modal" value="Cancelar">
            <input type="submit" class="btn btn-outline-primary btn-lg" value="Guardar">
          </div>
        </form>
      </div>
    </div>
  </div>