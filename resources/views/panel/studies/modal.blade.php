<!-- Modal -->
<div class="modal fade text-xs-left" id="modal-estudio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" id="modal-content-create">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Nuevo Estudio</h4>
        </div>
        <form class="form" method="POST" action="{{url('/studies')}}">
                {{ csrf_field() }}
            <input type="hidden" name="usuario_id" value="{{Auth::user()->id}}">
            <div class="modal-body">
                <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="estudio_institucion">Institución</label>
                                <div class="position-relative has-icon-left">
                                    <input type="text" id="estudio_institucion" class="form-control round" placeholder="Institución donde se dió la capacitación" name="estudio_institucion" required>
                                        <div class="form-control-position">
                                            <i class="icon-head"></i>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="estudio_carrera">Carrera</label>
                                <div class="position-relative has-icon-left">
                                    <input type="text" id="estudio_carrera" class="form-control round" placeholder="Institución donde se dió la capacitación" name="estudio_carrera" required>
                                        <div class="form-control-position">
                                            <i class="icon-head"></i>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group">
                                    <label class="display-inline-block custom-control custom-checkbox ml-1">
                                            <input type="checkbox" v-model="nacional" value="1" class="custom-control-input" id="estudio_nacional" name="estudio_nacional">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description ml-0">¿Estudio es en Ecuador?</span>
                                        </label>                                        
                            </div>                                      
                        </div>
                    </div>
                    </div>
                    <div class="row" v-show="nacional">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="select_provincia">Provincia</label>
                                <select id="select_provincia" class="form-control round" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Provincia" required>
                                    <option value="">--- Seleccionar ---</option>
                                    @foreach($provincias as $prov)
                                <option value="{{$prov->provincia_id}}">{{$prov->provincia_nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="select_canton">Cantón</label>
                                <select id="select_canton" name="canton_id" class="form-control round" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Cantón" required>
                                    <option value="">--- Seleccionar ---</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" v-show="!nacional">
                        <div class="col-md-12">
                            <div class="form-group">
                                    <label for="estudio_extranjero">Lugar</label>
                                    <div class="position-relative has-icon-left">
                                        <input type="text" id="estudio_extranjero" class="form-control round" placeholder="Lugar extranjero de estudio" name="estudio_extranjero">
                                            <div class="form-control-position">
                                                <i class="icon-head"></i>
                                            </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                                <label class="display-inline-block custom-control custom-checkbox ml-1">
                                                        <input type="checkbox" v-model="vigente" value="1" class="custom-control-input" id="estudio_vigente" name="estudio_vigente">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description ml-0">Estudio en curso</span>
                                                    </label>                                        
                                        </div>                                      
                                    </div>
                            </div>
                        </div>
                <div class="row">
                        <div class="col-md-3">
                                <div class="form-group">
                                    <label for="estudio_nivel">Nivel</label>
                                    <div class="position-relative has-icon-left">
                                            <select id="estudio_nivel" name="estudio_nivel" class="form-control round" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Tipo" required>
                                                <option value=3"">Pre-grado</option>
                                                <option value="4">Maestría/Diplomado/Doctorado</option>
                                                <option value="5">Phd</option>
                                            </select>
                                                <div class="form-control-position">
                                            <i class="icon-head"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <div class="col-md-3" v-show="vigente">
                        <div class="form-group">
                            <label for="estudio_semestre">Semestre</label>
                            <div class="position-relative has-icon-left">
                                    <select id="estudio_semestre" name="estudio_semestre" class="form-control round" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Semestre" required>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                        <option>10</option>
                                        <option>11</option>
                                        <option>12</option>
                                    </select>
                                        <div class="form-control-position">
                                    <i class="icon-head"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="estudio_inicio">Fecha de inicio</label>
                            <div class="position-relative has-icon-left">
                                <input type="date"  class="form-control round" id="estudio_inicio" name="estudio_inicio"  required>
                                <div class="form-control-position">
                                    <i class="icon-calendar5"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" v-show="!vigente">
                        <div class="form-group">
                            <label for="estudio_fin">Fecha de fin</label>
                            <div class="position-relative has-icon-left">
                                <input type="date"  class="form-control round" id="estudio_fin" name="estudio_fin">
                                <div class="form-control-position">
                                    <i class="icon-calendar5"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          </div>
          <div class="modal-footer">
            <input type="reset" class="btn btn-outline-warning btn-lg" data-dismiss="modal" value="Cancelar">
            <input type="submit" class="btn btn-outline-primary btn-lg" value="Guardar">
          </div>
        </form>
      </div>
    </div>
  </div>