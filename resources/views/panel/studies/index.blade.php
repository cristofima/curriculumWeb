@extends('layouts.panel')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
          <h2 class="content-header-title"><i class="icon-graduation-cap"></i> Estudios Universitarios</h2>
        </div>
        <div class="col-md-6 col-xs-12 mb-1">
            <button type="button" class="btn btn-info btn-min-width mr-1 mb-1" data-toggle="modal" data-target="#modal-estudio">Añadir</button>
        </div>
      </div>

      @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade in mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ $message }}
        </div>
    @endif

    @if (count($errors)>0)
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ $error }}
            </div>
        @endforeach
    @endif

    {{--  {{Request::route()->getName()}}  --}}

      <div class="content-body">
        <section>
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                        <h4 class="card-title">Estudios Universitarios</h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                                <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                                            </ul>
                                        </div>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block card-dashboard">
                                    <p>Listado de estudios</p>
                                </div>
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                                <tr>
                                                    <th>Institución</th>
                                                    <th>Especialización</th>
                                                    <th>Nivel</th>
                                                    <th>Ubicación</th>
                                                    <th>Periodo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($estudios as $est)
                                                    <tr>
                                                        <td>{{$est->estudio_institucion}}</td>
                                                        <td>{{$est->estudio_carrera}}</td>
                                                        <td> {{$est->estudio_nivel}}</td>
                                                        <td>
                                                            @if($est->estudio_extranjero == null)
                                                                {{$est->canton->canton_nombre}}, {{$est->canton->province->provincia_nombre}}
                                                            @else
                                                                {{$est->estudio_extranjero}}
                                                            @endif
                                                        </td>
                                                        <td>{{$est->estudio_inicio}} - {{$est->estudio_fin ?? 'Presente'}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                            @if(count($estudios) == 0)
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="5">No existen registros</td>
                                                    </tr>
                                                </tfoot>
                                            @endif
                                </table>           
                                {{$estudios->render()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
        </section>
      </div>
</div>
@include('panel.studies.modal')
@endsection
@push('styles')
    <link href="{{asset('css/select2.css')}}" rel="stylesheet"/>
@endpush
@push('scripts')
    <script src="{{asset('js/select2.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() { 

            $('#modal-estudio').on('show.bs.modal', function (event) {
                console.log('show modal');

                var modal = $(this);

                // modal.find("#select_provincia").select2({
                //     placeholder: "Seleccione una provincia",
                // }); 
                // modal.find("#select_provincia").select2({
                //     placeholder: "Seleccione un cantón",
                // });

                modal.find('#estudio_nacional').on('change',function(e){
                    if ($(this).is(':checked')) {
                        console.log("check nacional");
                        $(this).val(1);
                    } else {
                        console.log("no check nacional");
                        $(this).val(0);
                    }
                });

                modal.find('#estudio_vigente').on('change',function(e){
                    if ($(this).is(':checked')) {
                        console.log("check nacional");
                        $(this).val(1);
                    } else {
                        console.log("no check nacional");
                        $(this).val(0);
                    }
                });

                modal.find('#select_provincia').on('change',function(e){
                    console.log("change provincii");
                    var idProvincia=e.target.value;
                    if (idProvincia != "" && idProvincia != null) {
                        var op="";
                    $.ajax({
                        type:"GET",
                        url:"{!! URL::to('getCantons') !!}",
                        data:{
                        'idProvincia':idProvincia
                        },
                        success:function(data){
                        console.log('exito');
                        modal.find('#select_canton').empty();
                        op+="<option value=''>--- Seleccionar ---</option>";
                        $.each(data,function(index,obj){
                            var id=obj.canton_id;
                            var nombre=obj.canton_nombre;
                            op+="<option value='"+id+"'>"+nombre+"</option>";
                        });
                        modal.find('#select_canton').html('');
                        modal.find('#select_canton').append(op);
                
                        },
                        error:function(){
                            console.log('error');
                        }
                    });
                    }else{
                        modal.find('#select_canton').empty();
                        op="<option value=''>--- Seleccionar ---</option>";
                        modal.find('#select_canton').append(op);
                    }

                });
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
    
    </script>  

    <script src="{{asset('js/vue.js')}}"></script>
    <script type="text/javascript">

        new Vue({
            el: '#modal-content-create',
            data: {
                vigente: true,
                nacional:true
            }
        });
    </script>
@endpush