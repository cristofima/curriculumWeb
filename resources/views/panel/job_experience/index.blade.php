@extends('layouts.panel')
@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
          <h2 class="content-header-title"><i class="icon-institution"></i> Experiencia Laboral</h2>
        </div>
        <div class="col-md-6 col-xs-12 mb-1">
            <button type="button" class="btn btn-info btn-min-width mr-1 mb-1" data-toggle="modal" data-target="#large">Añadir</button>
        </div>
        
      </div>

     @include('includes.messages')

      <div class="content-body">
        <section>
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                        <h4 class="card-title">Experiencia Laboral</h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                                <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                                            </ul>
                                        </div>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block card-dashboard">
                                    <p>Listado de trabajos</p>
                                </div>
                                <div class="table-responsive">
                                    <table id="data-table" class="table table-striped table-bordered">
                                            <thead>
                                                    <tr>
                                                        <th>Institución</th>
                                                        <th>Cargo</th>
                                                        <th>Periodo</th>
                                                        <th>Tiempo</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   @foreach($experiencia as $exp)
                                                        <tr>
                                                            <td>{{$exp->experiencia_institucion}}</td>
                                                            <td>{{$exp->experiencia_cargo}}</td>
                                                            <td>{{$exp->experiencia_inicio}} - {{$exp->experiencia_fin ?? 'Presente'}}</td>
                                                            <td>{{$exp->getDiffTime()}}</td>
                                                        </tr>
                                                   @endforeach
                                                </tbody>
                                    </table>                              
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
        </section>
      </div>
</div>
@endsection
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('data_tables/datatables.min.css')}}"/>
@endpush
@push('scripts')
    <script type="text/javascript" src="{{asset('data_tables/datatables.min.js')}}"></script>
    <script>
    $('#data-table').DataTable( {
        responsive: true,
        select: true,
        dom: 'B<"clear">lfrtip',
        buttons: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        }
    } );
    </script>
@endpush