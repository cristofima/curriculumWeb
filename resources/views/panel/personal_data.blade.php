@extends('layouts.panel')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
          <h2 class="content-header-title"><i class="icon-user-secret"></i> Datos Personales</h2>
        </div>
      </div>

      @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade in mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ $message }}
        </div>
    @endif

    @if (count($errors)>0)
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ $error }}
            </div>
        @endforeach
    @endif

    {{--  {{Request::route()->getName()}}  --}}

      <div class="content-body">
        <section>
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                        <h4 class="card-title">Información</h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                                <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                                            </ul>
                                        </div>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block">
                                    <form id="form-personal-data" class="form" method="POST" action="{{url('/personal_data')}}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        @if ($personalData!=null)
                                            <input type="hidden" name="dato_id" value="{{$personalData->dato_id}}">
                                        @endif
                                        <input type="hidden" name="usuario_id" value="{{Auth::user()->id}}">
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="icon-head"></i> Información Personal</h4>
                                                    <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Nombres</label>
                                                                    <div class="position-relative has-icon-left">
                                                                    <input type="text"  class="form-control round" placeholder="Nombres" value="{{$personalData!=null?$personalData->dato_nombres:old('dato_nombres')}}" name="dato_nombres" required>
                                                                            <div class="form-control-position">
                                                                                <i class="icon-head"></i>
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput2">Apellidos</label>
                                                                    <div class="position-relative has-icon-left">
                                                                            <input type="text"  class="form-control round" placeholder="Apellidos" value="{{$personalData!=null?$personalData->dato_apellidos:old('dato_apellidos')}}" name="dato_apellidos" required>
                                                                            <div class="form-control-position">
                                                                                <i class="icon-head"></i>
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                        </div>
                    
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Cédula</label>
                                                                    <div class="position-relative has-icon-left">
                                                                            <input type="text" class="form-control round" placeholder="Cédula" value="{{$personalData!=null?$personalData->dato_cedula:old('dato_cedula')}}" name="dato_cedula" required>
                                                                            <div class="form-control-position">
                                                                                <i class="icon-head"></i>
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput2">Fecha de nacimiento</label>
                                                                    <div class="position-relative has-icon-left">
                                                                        <input type="date"  class="form-control round" name="dato_fecha_nacimiento" value="{{$personalData!=null?$personalData->dato_fecha_nacimiento:old('dato_fecha_nacimiento')}}" required>
                                                                        <div class="form-control-position">
                                                                            <i class="icon-calendar5"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <h4 class="form-section"><i class="icon-android-pin"></i> Residencia</h4>

                                                        <div class="row">

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="issueinput5">Provincia</label>
                                                                    <select id="select_provincia"  @change="changedProvince" v-model="id_provincia" class="form-control round" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Provincia" required>
                                                                        @foreach($provincias as $prov)
                                                                            <option value="{{$prov->provincia_id}}" {{$personalData!=null ? $personalData->canton->province->provincia_id==$prov->provincia_id?'selected':'' : old('provincia_id')==$prov->provincia_id?'selected':''}}>{{$prov->provincia_nombre}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="issueinput5">Cantón</label>
                                                                    <select id="select_canton" name="canton_id" class="form-control round" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Cantón" required>
                                                                        @if ($personalData!=null)
                                                                            @foreach($personalData->canton->province->cantons as $cant)
                                                                                <option value="{{$cant->canton_id}}" {{$personalData->canton->canton_id==$cant->canton_id?'selected':''}}>{{$cant->canton_nombre}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        <h4 class="form-section"><i class="icon-mail6"></i> Contactos & Biografía</h4>
                    
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Dirección</label>
                                                                    <div class="position-relative has-icon-left">
                                                                            <input type="text"  class="form-control round" placeholder="Dirección" name="dato_direccion" value="{{$personalData!=null?$personalData->dato_direccion:old('dato_direccion')}}" required>
                                                                            <div class="form-control-position">
                                                                                <i class="icon-head"></i>
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput2">Celular</label>
                                                                    <div class="position-relative has-icon-left">
                                                                            <input type="text"  class="form-control round" placeholder="Celular" name="dato_celular" value="{{$personalData!=null?$personalData->dato_celular:old('dato_celular')}}" required>
                                                                            <div class="form-control-position">
                                                                                <i class="icon-ios-telephone"></i>
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="complaintinput5">Perfil profesional</label>
                                                                <textarea id="complaintinput5" rows="5" class="form-control round" name="dato_perfil" placeholder="Perfil profesional">{{$personalData->dato_perfil}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="complaintinput5">Email</label>
                                                                    <input type="text" class="form-control round" name="dato_email" placeholder="E-mail" value="{{$personalData!=null?$personalData->dato_email:old('dato_email')}}" required />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="complaintinput5">Foto</label>
                                                                    <input type="file" class="form-control round" name="dato_foto"/>
                                                                </div>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                
                                                <div class="form-actions">
                                                        <button type="reset" class="btn btn-warning mr-1">
                                                            <i class="icon-cross2"></i> Resetear
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">
                                                            <i class="icon-check2"></i> Guardar
                                                        </button>
                                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
        </section>
      </div>
</div>
@endsection
@push('styles')
    <link href="{{asset('css/select2.css')}}" rel="stylesheet"/>
@endpush
@push('scripts')
<script src="{{asset('js/select2.js')}}"></script>
<script src="{{asset('js/vue.js')}}"></script>
<script type="text/javascript">

        new Vue({
            el: '#form-personal-data',
            data: {
                id_provincia: null
            },
            methods: {
                changedProvince: function(value) {
                    //receive the value selected (return an array if is multiple)
                    if (value != "" && value != null){
                        this.$http.get("{!! URL::to('getCantons') !!}",{
                            'idProvincia':value
                        }).then(response=>{
                            // get body data
                            this.cantons = response.body;
                            var op="";
                            console.log('exito');
                            $('#select_canton').empty();
                            op+="<option value=''>--- Seleccionar ---</option>";
                            $.each(cantons,function(index,obj){
                                var id=obj.canton_id;
                                var nombre=obj.canton_nombre;
                                op+="<option value='"+id+"'>"+nombre+"</option>";
                            });
                            $('#select_canton').html('');
                            $('#select_canton').append(op);
                        }, error=>{
                            console.error(error);
                        });
                    }
                }
            }
        });
</script>

<script type="text/javascript">
    $(document).ready(function() { 
        $("#select_provincia").select2({
            placeholder: "Seleccione una provincia",
        }); 
        $("#select_canton").select2({
            placeholder: "Seleccione un cantón",
        });
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#select_provincia').on('change',function(e){
    var idProvincia=e.target.value;
    if (idProvincia != "" && idProvincia != null) {
        var op="";
      $.ajax({
        type:"GET",
        url:"{!! URL::to('getCantons') !!}",
        data:{
          'idProvincia':idProvincia
        },
        success:function(data){
          console.log('exito');
          $('#select_canton').empty();
          op+="<option value=''>--- Seleccionar ---</option>";
           $.each(data,function(index,obj){
             var id=obj.canton_id;
             var nombre=obj.canton_nombre;
             op+="<option value='"+id+"'>"+nombre+"</option>";
           });
           $('#select_canton').html('');
           $('#select_canton').append(op);
        },
        error:function(){
            console.log('error');
        }
      });
    }else{
        $('#select_canton').empty();
      op="<option value=''>--- Seleccionar ---</option>";
      $('#select_canton').append(op);
    }
});

</script>    
@endpush