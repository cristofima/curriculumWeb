@extends('layouts.app')
@section('content')
<div class="card border-grey border-lighten-3 m-0">
            <div class="card-header no-border">
                <div class="card-title text-xs-center">
                    <div class="p-1"><img src="{{ asset('images/logo/robust-logo-dark.png')}}" alt="branding logo"></div>
                </div>
                <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Iniciar Sesión</span></h6>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    @if (session('confirmation-success'))
                        <div class="alert alert-success alert-dismissible fade in mb-2" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
                            {{ session('confirmation-success') }}
						</div>
                    @endif
                    @if (session('confirmation-danger'))
                        <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    	<span aria-hidden="true">&times;</span>
							</button>
                            {!! session('confirmation-danger') !!}
						</div>
                    @endif
                    <form class="form-horizontal form-simple" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <fieldset class="form-group{{ $errors->has('email') ? ' has-error' : '' }} position-relative has-icon-left mb-0">
                            <input  id="email" type="email" class="form-control form-control-lg input-lg" name="email" value="{{ old('email') }}" required autofocus placeholder="E-mail">
                            <div class="form-control-position">
                                <i class="icon-mail6"></i>
                            </div>
                            @if ($errors->has('email'))
                                <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<strong>{{ $errors->first('email') }}</strong>
								</div>
                            @endif
                        </fieldset>
                       
                        <fieldset class="form-group{{ $errors->has('password') ? ' has-error' : '' }} position-relative has-icon-left">
                            <input id="password" type="password" class="form-control form-control-lg input-lg" name="password" required placeholder="Contraseña">
                            <div class="form-control-position">
                                <i class="icon-key3"></i>
                            </div>
                            @if ($errors->has('password'))
                                <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<strong>{{ $errors->first('password') }}</strong>
								</div>
                            @endif
                        </fieldset>
                        <fieldset class="form-group row">
                            <div class="col-md-6 col-xs-12 text-xs-center text-md-left">
                                <fieldset>
                                    <input type="checkbox" id="remember-me" name="remember" {{ old('remember') ? 'checked' : '' }} class="chk-remember">
                                    <label for="remember-me"> Recordar contraseña</label>
                                </fieldset>
                            </div>
                            <div class="col-md-6 col-xs-12 text-xs-center text-md-right"><a href="{{ url('/password/reset') }}" class="card-link">¿Olvidastes tu contraseña?</a></div>
                        </fieldset>
                        <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="icon-unlock2"></i> Acceder</button>
                    </form>
                </div>
            </div>
            <div class="card-footer">
                <div class="">
                    <p class="float-sm-right text-xs-center m-0">¿Eres nuevo? <a href="{{ url('/register') }}" class="card-link">Regístrate</a></p>
                </div>
            </div>
        </div>
@endsection
