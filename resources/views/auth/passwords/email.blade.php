@extends('layouts.app')

@section('content')
<div class="card border-grey border-lighten-3 px-2 py-2 m-0">
            <div class="card-header no-border pb-0">
                <div class="card-title text-xs-center">
                    <img src="{{ asset('images/logo/robust-logo-dark.png') }}" alt="branding logo">
                </div>
                <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Le enviaremos un enlace para restablecer su contraseña</span></h6>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                     @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade in mb-2" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
                            {{ session('status') }}
						</div>
                    @endif
                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <fieldset class="form-group position-relative has-icon-left">
                            <input placeholder="E-mail" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-lg input-lg" name="email" value="{{ old('email') }}" required>
                            <div class="form-control-position">
                                <i class="icon-mail6"></i>
                            </div>
                            @if ($errors->has('email'))
                                <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </fieldset>
                        <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="icon-lock4"></i> Recuperar contraseña</button>
                    </form>
                </div>
            </div>
            <div class="card-footer no-border">
                <p class="float-sm-left text-xs-center"><a href="{{url('/login')}}" class="card-link">Iniciar Sesión</a></p>
                <p class="float-sm-right text-xs-center">¿Eres nuevo? <a href="{{url('/register')}}" class="card-link">Crear cuenta</a></p>
            </div>
        </div>
@endsection
