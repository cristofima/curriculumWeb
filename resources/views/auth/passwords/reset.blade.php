@extends('layouts.app')

@section('content')
<div class="card border-grey border-lighten-3 px-2 py-2 m-0">
    <div class="card-header no-border">
				<div class="card-title text-xs-center">
					<img src="{{ asset('images/logo/robust-logo-dark.png')}}" alt="branding logo">
				</div>
				<h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Resetear contraseña</span></h6>
			</div>
    <div class="card-body collapse in">
        <div class="card-block">
                    <form method="POST" class="form-horizontal form-simple" action="{{ route('password.request') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <fieldset class="form-group{{ $errors->has('email') ? ' has-error' : '' }} position-relative has-icon-left mb-1">
							<input type="text" placeholder="E-mail" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-lg input-lg" name="email" value="{{ $email or old('email') }}" required autofocus>
							<div class="form-control-position">
							    <i class="icon-mail6"></i>
							</div>
                            @if ($errors->has('email'))
                                <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>{{ $errors->first('email') }}</strong>
                                </div>
                            @endif
						</fieldset>

                        <fieldset class="form-group{{ $errors->has('password') ? ' has-error' : '' }} position-relative has-icon-left">
							<input type="password" placeholder="Contraseña" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-lg input-lg" name="password" required>
							<div class="form-control-position">
							    <i class="icon-key3"></i>
							</div>
                            @if ($errors->has('password'))
                                <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>{{ $errors->first('password') }}</strong>
                                </div>
                            @endif
						</fieldset>

                        <fieldset class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} position-relative has-icon-left">
							<input type="password" placeholder="Confirmar contraseña" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }} form-control-lg input-lg" name="password_confirmation" required>
							<div class="form-control-position">
							    <i class="icon-key3"></i>
							</div>
                            @if ($errors->has('password_confirmation'))
                                <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </div>
                            @endif
						</fieldset>

                        <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="icon-unlock2"></i> Resetear contraseña</button>
                    </form>
        </div>
        <p class="text-xs-center">¿Ya tienes una cuenta? <a href="{{ url('/login') }}" class="card-link">Iniciar Sesión</a></p>
    </div>
</div>
@endsection
