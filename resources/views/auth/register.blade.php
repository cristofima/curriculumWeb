@extends('layouts.app')

@section('content')
<div class="card border-grey border-lighten-3 px-2 py-2 m-0">
			<div class="card-header no-border">
				<div class="card-title text-xs-center">
					<img src="{{ asset('images/logo/robust-logo-dark.png')}}" alt="branding logo">
				</div>
				<h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Crear cuenta</span></h6>
			</div>
			<div class="card-body collapse in">	
				<div class="card-block">
                    @if (session('confirmation-success'))
                        <div class="alert alert-success alert-dismissible fade in mb-2" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
                            {{ session('confirmation-success') }}
						</div>
                    @else
					<form class="form-horizontal form-simple" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}
						<fieldset class="form-group{{ $errors->has('name') ? ' has-error' : '' }} position-relative has-icon-left mb-1">
							<input type="text" placeholder="Nombre de usuario" class="form-control form-control-lg input-lg" name="name" value="{{ old('name') }}" required autofocus>
							<div class="form-control-position">
							    <i class="icon-head"></i>
							</div>
                            @if ($errors->has('name'))
                                <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>{{ $errors->first('name') }}</strong>
                                </div>
                            @endif
						</fieldset>

						<fieldset class="form-group{{ $errors->has('email') ? ' has-error' : '' }} position-relative has-icon-left mb-1">
							<input type="email" placeholder="E-mail" class="form-control form-control-lg input-lg" name="email" value="{{ old('email') }}" required>
							<div class="form-control-position">
							    <i class="icon-mail6"></i>
							</div>
                            @if ($errors->has('email'))
                                <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>{{ $errors->first('email') }}</strong>
                                </div>
                            @endif
						</fieldset>

						<fieldset class="form-group{{ $errors->has('password') ? ' has-error' : '' }} position-relative has-icon-left">
							<input type="password" placeholder="Contraseña" class="form-control form-control-lg input-lg" name="password" required>
							<div class="form-control-position">
							    <i class="icon-key3"></i>
							</div>
                            @if ($errors->has('password'))
                                <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>{{ $errors->first('password') }}</strong>
                                </div>
                            @endif
						</fieldset>

                        <fieldset class="form-group position-relative has-icon-left">
							<input type="password" placeholder="Confirmar contraseña" class="form-control form-control-lg input-lg" name="password_confirmation" required>
							<div class="form-control-position">
							    <i class="icon-key3"></i>
							</div>
						</fieldset>
						<button type="submit" class="btn btn-primary btn-lg btn-block"><i class="icon-unlock2"></i> Registrarse</button>
					</form>
                    @endif
				</div>
				<p class="text-xs-center">¿Ya tienes una cuenta? <a href="{{ url('/login') }}" class="card-link">Iniciar Sesión</a></p>
			</div>
		</div>
@endsection
