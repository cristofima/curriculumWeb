<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Email confirmation 
Route::get('confirmation/resend', 'Auth\RegisterController@resend');
Route::get('confirmation/{id}/{token}', 'Auth\RegisterController@confirm');
Route::get('capacitations/chart', 'CV_CapacitacionController@chart');

// Grupo de rutas para el panel de administración
Route::group(['middleware' => 'auth'], function () {
    Route::get('dashboard', 'HomeController@index')->name('dashboard');
    Route::get('getCantons', 'GEN_LocationController@getCantons');
    Route::get('account', 'CV_UserController@account');
    Route::get('curriculum', 'PDFController@index');

    Route::post('user_profile', 'CV_UserController@update_profile');
    Route::post('user_password', 'CV_UserController@update_password');
   
    Route::resource('personal_data', 'CV_DatosPersonalesController', ['only' => ['index', 'store','update']]);
    Route::resource('job_experience', 'CV_ExperienciaController', ['only' => ['index', 'store','update']]);
    Route::resource('capacitations', 'CV_CapacitacionController', ['only' => ['index', 'store','update']]);
    Route::resource('acknowledgements', 'CV_MeritoController', ['only' => ['index', 'store','update']]);
    Route::resource('studies', 'CV_EstudioController', ['only' => ['index', 'store','update']]);
    Route::resource('languajes', 'CV_IdiomaController', ['only' => ['index', 'store','update']]);
    Route::resource('presentations', 'CV_PonenciaController', ['only' => ['index', 'store','update']]);
    Route::resource('publications', 'CV_PublicacionController', ['only' => ['index', 'store','update']]);
    Route::resource('certifications', 'CV_CertificacionController', ['only' => ['index', 'store','update']]);
});
