<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    public $timestamps = true;
    
    const CREATED_AT = 'usuario_fecha_registro';
    const UPDATED_AT = 'usuario_ultima_actualizacion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','confirmed','confirmation_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','confirmation_code'
    ];

    public function personalData() {
        return $this->hasOne('App\CV_DatosPersonales', 'usuario_id');
    }

    public function capacitations() {
        return $this->hasMany('App\CV_Capacitacion', 'usuario_id');
    }

    public function studies() {
        return $this->hasMany('App\CV_Estudio', 'usuario_id');
    }

    public function languajes() {
        return $this->hasMany('App\CV_Idioma', 'usuario_id');
    }

    public function acknowledgements() {
        return $this->hasMany('App\CV_Merito', 'usuario_id');
    }

    public function experiencie() {
        return $this->hasMany('App\CV_Experiencia', 'usuario_id');
    }

    public function presentations() {
        return $this->hasMany('App\CV_Ponencia', 'usuario_id');
    }

    public function publications() {
        return $this->hasMany('App\CV_Publicacion', 'usuario_id');
    }

    public function certifications() {
        return $this->hasMany('App\CV_Certificacion', 'usuario_id');
    }

}
