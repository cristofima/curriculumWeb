<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CV_Certificacion extends Model{
    
    protected $table = "cv_certificaciones";
    protected $primaryKey = "certificacion_id";

    public $timestamps = false;

    protected $guarded=[];

    public function user() {
        return $this->belongsTo('App\User', 'usuario_id');
    }
}
