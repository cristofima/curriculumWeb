<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GEN_Idioma extends Model
{

    protected $table = "gen_idiomas";
    protected $primaryKey = "idioma_id";
    public $timestamps = false;
}
