<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CV_Estudio extends Model{
    
    protected $table = "cv_estudios";
    protected $primaryKey = "estudio_id";

    public $timestamps = false;

    protected $guarded=[];

    public function user() {
        return $this->belongsTo('App\User', 'usuario_id');
    }

    public function canton() {
        return $this->belongsTo('App\GEN_Canton', 'canton_id');
    }
}
