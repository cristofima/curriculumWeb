<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CV_Idioma extends Model{
    
    protected $table = "cv_idiomas";
    protected $primaryKey = "idiomaext_id";

    public $timestamps = false;

    protected $guarded=[];

    public function languaje() {
        return $this->belongsTo('App\GEN_Idioma', 'idioma_id');
    }

    public function scopeGetName($query,$value){
        if($value != null && $value != ""){
            if($value == 'B'){
                return "Básico";
            }else if($value == 'I'){
                return "Intermedio";
            }else if($value == 'A'){
                return "Avanzado";
            }else{
                return "Nativo";
            }
        }else{
            return null;
        }
    }
}
