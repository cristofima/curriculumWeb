<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GEN_Provincia extends Model
{

    protected $table = "gen_provincias";
    protected $primaryKey = "provincia_id";
    public $timestamps = false;

    public function cantons()
    {
        return $this->hasMany('App\GEN_Canton', 'provincia_id');
    }
}
