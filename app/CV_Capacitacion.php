<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Date;

class CV_Capacitacion extends Model{
    
    protected $table = "cv_capacitaciones";
    protected $primaryKey = "capacitacion_id";

    public $timestamps = false;

    protected $guarded=[];

    public function user() {
        return $this->belongsTo('App\User', 'usuario_id');
    }

    public function scopeGetYearCapacitation(){
        if($this->capacitacion_fin != null){
            $date= new Date($this->capacitacion_fin);
            return $date->year;
        }
    }

    public function scopeGetTypeCapacitation(){
        $tipo=$this->capacitacion_tipo; 
        if($tipo=='CU'){
            return 'Curso';
        }else if($tipo=='TA'){
            return 'Taller';
        }else if($tipo=='SE'){
            return 'Seminario';
        }else if($tipo=='JO'){
            return 'Jornada';
        }else if($tipo=='CO'){
            return 'Congreso';
        }
    }
}
