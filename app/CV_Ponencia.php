<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CV_Ponencia extends Model{
    
    protected $table = "cv_ponencias";
    protected $primaryKey = "ponencia_id";

    public $timestamps = false;

    protected $guarded=[];

    public function user() {
        return $this->belongsTo('App\User', 'usuario_id');
    }
}
