<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Date;

class CV_Experiencia extends Model{
    
    protected $table = "cv_experiencia_laboral";
    protected $primaryKey = "experiencia_id";

    public $timestamps = false;

    protected $guarded=[];

    public function user() {
        return $this->belongsTo('App\User', 'usuario_id');
    }

    public function scopeGetDiffTime($query){
        Date::setLocale('en');
        $rang = null;
        $dateInicio = new Date($this->experiencia_inicio);
        if($this->experiencia_fin!=null){
            $dateFin = new Date($this->experiencia_fin);
            $rang=$dateInicio->timespan($dateFin);
        }else{
            $rang=Date::now()->timespan($dateInicio);
        }
        $tiempo_experiencia=[];
        $tiempo_experiencia['years']=0;
        $tiempo_experiencia['months']=0;
        $tiempo_experiencia['weeks']=0;
        $array=preg_split('/,/',$rang);
        foreach($array as $arr){
            $arr=trim($arr);
            if(strpos($arr,'year')!= false){                    
                $tiempo_experiencia['years']+=intval(preg_replace('/\s+(\w+)/',"",$arr));
            }else if(strpos($arr,'month')!= false){
                $tiempo_experiencia['months']+=intval(preg_replace('/\s+(\w+)/',"",$arr));
            }else if(strpos($arr,'week')!= false){
                $tiempo_experiencia['weeks']+=intval(preg_replace('/\s+(\w+)/',"",$arr));
            }
        }
        if($tiempo_experiencia['weeks']>=4){
            $tiempo_experiencia['weeks']-=4;
            $tiempo_experiencia['months']+=1;
        }
        if($tiempo_experiencia['months']>=12){
            $tiempo_experiencia['months']-=12;
            $tiempo_experiencia['years']+=1;
        }
        \Log::info($tiempo_experiencia);
        $experience=null;
        if($tiempo_experiencia['years']>0){
            $experience=$tiempo_experiencia['years']." años";
        }
        if($tiempo_experiencia['months']>0){
            $experience.=" ".$tiempo_experiencia['months']." meses";
        }
        if($tiempo_experiencia['weeks']>0){
            $experience.=" ".$tiempo_experiencia['weeks']." semanas";
        }
        return $experience;
    }
}
