<?php

namespace App\Providers;

use Validator;
use Auth;
use Hash;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(){
        $this->validePassword();
        $this->validateCurrentPassword();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    private function validateCurrentPassword() {
        Validator::extend('current_password', function($attribute, $value, $parametes) {
          return Hash::check($value, Auth::user()->password);
        });
    }

    public function validePassword() {
        Validator::extend('password', function($attribute, $value, $parametes) {
          if (preg_match('/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])[A-Za-z0-9_#@%\*\-]{8,25}$/', $value)) {
            return true;
          }
          return false;
        });
      }
}
