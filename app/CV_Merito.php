<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CV_Merito extends Model{
    
    protected $table = "cv_meritos";
    protected $primaryKey = "merito_id";

    public $timestamps = false;

    protected $guarded=[];

    public function user() {
        return $this->belongsTo('App\User', 'usuario_id');
    }
}
