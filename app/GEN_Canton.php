<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GEN_Canton extends Model
{

    protected $table = "gen_cantones";
    protected $primaryKey = "canton_id";
    public $timestamps = false;

    public function province()
    {
        return $this->belongsTo('App\GEN_Provincia', 'provincia_id');
    }
}
