<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CV_DatosPersonales extends Model{
    
    protected $table = "cv_datos_personales";
    protected $primaryKey = "dato_id";

    public $timestamps = true;
    
    const CREATED_AT = 'dato_fecha_registro';
    const UPDATED_AT = 'dato_ultima_actualizacion';

    protected $fillable = [
        'usuario_id', 'canton_id', 'dato_cedula','dato_nombres','dato_apellidos','dato_fecha_nacimiento',
        'dato_lugar_nacimiento','dato_direccion','dato_celular','dato_email','dato_perfil','dato_foto'
    ];

    public function user() {
        return $this->belongsTo('App\User', 'usuario_id');
    }

    public function canton() {
        return $this->belongsTo('App\GEN_Canton', 'canton_id');
    }

    public static function boot(){
        parent::boot();
    }
}
