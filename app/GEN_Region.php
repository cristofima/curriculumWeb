<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GEN_Region extends Model
{

    protected $table = "gen_regiones";
    protected $primaryKey = "region_id";
    public $timestamps = false;

    public function provinces()
    {
        return $this->hasMany('App\GEN_Provincia', 'region_id');
    }
}
