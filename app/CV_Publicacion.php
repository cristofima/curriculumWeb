<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CV_Publicacion extends Model{
    
    protected $table = "cv_publicaciones";
    protected $primaryKey = "publicacion_id";

    public $timestamps = false;

    protected $guarded=[];

    public function user() {
        return $this->belongsTo('App\User', 'usuario_id');
    }

    public function scopeGetTypePublication(){
        $tipo=$this->publicacion_tipo; 
        if($tipo=='A'){
            return 'Artículo';
        }else if($tipo=='L'){
            return 'Libro';
        }
    }
}
