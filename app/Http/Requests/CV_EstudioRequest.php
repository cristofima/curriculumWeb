<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CV_EstudioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'usuario_id'=>'required|numeric|exists:users,id',
            'canton_id'=>'required_without:estudio_nacional|numeric|exists:gen_cantones,canton_id',
            'estudio_institucion'=>'required|string|max:100|min:5',
            'estudio_carrera'=>'required|string|max:50|min:10',
            'estudio_vigente'=>'nullable|boolean',
            'estudio_nivel'=>'required|numeric|min:3|max:5',
            'estudio_semestre'=>'required_with:estudio_vigente|numeric|min:1|max:12',
            'estudio_inicio'=>'required|date',
            'estudio_fin'=>'required_without:estudio_vigente',
            'estudio_extranjero'=>'required_without:estudio_nacional',
            'estudio_nacional'=>'nullable|boolean'
            
        ];
    }
}
