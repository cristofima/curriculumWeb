<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CV_CapacitacionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'usuario_id'=>'required|numeric|exists:users,id',
            'capacitacion_nombre'=>'required|string|max:100|min:5',
            'capacitacion_tipo'=>'required|string|in:CU,TA,SE,JO,CO',
            'capacitacion_cargo'=>'required|string|in:OR,AS,VO,FA',
            'capacitacion_institucion'=>'required|string|max:100|min:5',
            'capacitacion_inicio'=>'required|date',
            'capacitacion_fin'=>'nullable|date',
            'capacitacion_horas'=>'required|numeric|min:1'
        ];
    }
}
