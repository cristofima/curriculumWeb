<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Input;

class CV_DatosPersonalesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        if(\Input::has('dato_id')){
            $dato_id=Input::get('dato_id');
            return [
                'dato_id'=>'required|exists:cv_datos_personales,dato_id',
                'usuario_id'=>'required|numeric|exists:users,id',
                'canton_id'=>'required|numeric|exists:gen_cantones,canton_id',
                'dato_cedula'=>'required|string|size:10|unique:cv_datos_personales,dato_cedula,'.$dato_id.',dato_id',
                'dato_nombres'=>'required|string|max:30|min:5',
                'dato_apellidos'=>'required|string|max:30|min:5',
                'dato_fecha_nacimiento'=>'required|date',
                'dato_direccion'=>'required|string|max:50|min:5',
                'dato_celular'=>'required|string|size:10|unique:cv_datos_personales,dato_celular,'.$dato_id.',dato_id',
                'dato_perfil'=>'nullable|string',
                'dato_email'=>'required|email|max:50',
                'dato_foto'=>'nullable|file|mimes:png,jpg,jpeg'
            ];
        }else{
            return [
                'usuario_id'=>'required|numeric|exists:users,id',
                'canton_id'=>'required|numeric|exists:gen_cantones,canton_id',
                'dato_cedula'=>'required|string|size:10|unique:cv_datos_personales',
                'dato_nombres'=>'required|string|max:30|min:5',
                'dato_apellidos'=>'required|string|max:30|min:5',
                'dato_fecha_nacimiento'=>'required|date',
                'dato_direccion'=>'required|string|max:50|min:5',
                'dato_celular'=>'required|string|size:10|unique:cv_datos_personales',
                'dato_perfil'=>'nullable|string',
                'dato_email'=>'required|email|max:50',
                'dato_foto'=>'nullable|file|mimes:png,jpg,jpeg'
            ];
        }
    }
}
