<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CV_IdiomaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'usuario_id'=>'required|numeric|exists:users,id',
            'idioma_id'=>'required|numeric|exists:gen_idiomas,idioma_id',
            'idiomaext_nivel_escrito'=>'required|in:B,I,A,N',
            'idiomaext_nivel_hablado'=>'required|in:B,I,A,N',
            'idiomaext_nivel_leido'=>'required|in:B,I,A,N'
        ];
    }
}
