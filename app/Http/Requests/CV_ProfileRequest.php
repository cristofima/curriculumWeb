<?php

namespace App\Http\Requests;

use Input;
use Illuminate\Foundation\Http\FormRequest;

class CV_ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        $id=Input::get('id');
        return [
            'id'=>'required|numeric|exists:users,id',
            'email'=>'required|email|max:50|unique:users,email,'.$id.',id',
            'name'=>'required|string|min:3|max:20|unique:users,name,'.$id.',id'
        ];
    }
}


