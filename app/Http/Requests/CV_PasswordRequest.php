<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CV_PasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'id'=>'required|numeric|exists:users,id',
            'current_password'=>'required|min:8|max:50|current_password|password',
            'new_password'=>'required|password|min:8|max:20|confirmed'
        ];
    }
}
