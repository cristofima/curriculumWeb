<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CV_MeritoRequest extends FormRequest{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'usuario_id'=>'required|numeric|exists:users,id',
            'merito_descripcion'=>'required|string|max:200|min:5',
            'merito_institucion'=>'required|string|max:100|min:5',
            'capacitacion_fin'=>'nullable|date'
        ];
    }
}
