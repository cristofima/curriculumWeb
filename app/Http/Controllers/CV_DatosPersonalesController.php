<?php

namespace App\Http\Controllers;

use App\CV_DatosPersonales;
use App\GEN_Provincia;
use App\Http\Requests\CV_DatosPersonalesRequest;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CV_DatosPersonalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personalData = Auth::user()->personalData;
        $provincias = GEN_Provincia::orderBy('provincia_id')->get();
        return view('panel.personal_data', compact('personalData', 'provincias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Requests\CV_DatosPersonalesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CV_DatosPersonalesRequest $request)
    {
        if ($request->dato_id) {
            /*$datos=CV_DatosPersonales::findOrFail($request->dato_id);
            if(Input::hasFile('dato_foto')){
            $image = Input::file('dato_foto');
            $datos->dato_foto=base64_encode(file_get_contents($image->getRealPath()));
            }*/
            CV_DatosPersonales::updateOrCreate(['dato_id' => $request->dato_id], $request->except(['dato_id', '_token']));
        } else {
            CV_DatosPersonales::create($request->except(['_token']));
        }
        return Redirect::to('personal_data')->with('success', 'Datos personales guardados');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CV_DatosPersonales  $cV_DatosPersonales
     * @return \Illuminate\Http\Response
     */
    public function show(CV_DatosPersonales $cV_DatosPersonales)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CV_DatosPersonales  $cV_DatosPersonales
     * @return \Illuminate\Http\Response
     */
    public function edit(CV_DatosPersonales $cV_DatosPersonales)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CV_DatosPersonales  $cV_DatosPersonales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CV_DatosPersonales $cV_DatosPersonales)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CV_DatosPersonales  $cV_DatosPersonales
     * @return \Illuminate\Http\Response
     */
    public function destroy(CV_DatosPersonales $cV_DatosPersonales)
    {
        //
    }
}
