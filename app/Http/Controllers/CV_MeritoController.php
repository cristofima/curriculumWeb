<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\CV_Merito;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\CV_MeritoRequest;

class CV_MeritoController extends Controller{
    
    public function index(){
        $meritos=Auth::user()->acknowledgements()->select(DB::raw('EXTRACT(YEAR FROM merito_fecha) as year'))
        ->orderBy('year','DESC')->distinct()->get();
        $meses=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        return view('panel.acknowledgements.index',compact('meritos','meses'));
    }

    public function store(CV_MeritoRequest $request){
        CV_Merito::create($request->all());
        return Redirect::to('acknowledgements')->with('success', 'Mérito guardado');
    }
}
