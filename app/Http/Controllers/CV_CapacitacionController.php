<?php

namespace App\Http\Controllers;

use App\CV_Capacitacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\CV_CapacitacionRequest;

use Auth;
use DB;

class CV_CapacitacionController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $capacitaciones=Auth::user()->capacitations()->orderBy('capacitacion_inicio','DESC')->get();
        return view('panel.capacitations.index',compact('capacitaciones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CV_CapacitacionRequest $request){
        CV_Capacitacion::create($request->except('_token'));
        return Redirect::to('capacitations')->with('success', 'Capacitación guardada');
    }

    public function chart(){
        $result=CV_Capacitacion::select(DB::raw('EXTRACT(YEAR FROM capacitacion_inicio) as year,sum(capacitacion_horas) as hours,capacitacion_tipo'))
        ->orderBy('year')->groupBy('year','capacitacion_tipo')->get();
        return response()->json($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CV_Capacitaciones  $cV_Capacitaciones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CV_Capacitaciones $cV_Capacitaciones)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CV_Capacitaciones  $cV_Capacitaciones
     * @return \Illuminate\Http\Response
     */
    public function destroy(CV_Capacitaciones $cV_Capacitaciones)
    {
        //
    }
}
