<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Date;

class HomeController extends Controller{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $capacitaciones_horas=Auth::user()->capacitations()->sum('capacitacion_horas');
        $mejores_capacitaciones=Auth::user()->capacitations()
        ->orderByRaw('capacitacion_horas desc,capacitacion_fin desc')
        ->take(10)->get();
        $meritos=Auth::user()->acknowledgements()->count();
        $experiencia=Auth::user()->experiencie()->orderBy('experiencia_fin')->get();
        $lenguajes=Auth::user()->languajes()->count();
        $ponencias=Auth::user()->presentations()->count();
        $publicaciones=Auth::user()->publications()->count();
        $certificaciones=Auth::user()->certifications()->count();
        $estudios=Auth::user()->studies()->where('estudio_fin','<>',null)->count();
        Date::setLocale('en');
        $diffTime = [];
        foreach($experiencia as $exp){
            $dateInicio = new Date($exp->experiencia_inicio);
            if($exp->experiencia_fin!=null){
                $dateFin = new Date($exp->experiencia_fin);
                array_push($diffTime,$dateInicio->timespan($dateFin));
            }else{
                array_push($diffTime,Date::now()->timespan($dateInicio));
            }
        }
        $tiempo_experiencia=$this->getDiffTime($diffTime);
        return view('panel.dashboard',compact('mejores_capacitaciones','estudios','certificaciones','publicaciones','ponencias','capacitaciones_horas','meritos','lenguajes','tiempo_experiencia'));
    }

    private function getDiffTime($ranges){
        $tiempo_experiencia=[];
        $tiempo_experiencia['years']=0;
        $tiempo_experiencia['months']=0;
        $tiempo_experiencia['weeks']=0;
        foreach($ranges as $rang){
            $array=preg_split('/,/',$rang);
            foreach($array as $arr){
                $arr=trim($arr);
                if(strpos($arr,'year')!= false){                    
                    $tiempo_experiencia['years']+=intval(preg_replace('/\s+(\w+)/',"",$arr));
                }else if(strpos($arr,'month')!= false){
                    $tiempo_experiencia['months']+=intval(preg_replace('/\s+(\w+)/',"",$arr));
                }else if(strpos($arr,'week')!= false){
                    $tiempo_experiencia['weeks']+=intval(preg_replace('/\s+(\w+)/',"",$arr));
                }
            }
        }
        if($tiempo_experiencia['weeks']>=4){
            $tiempo_experiencia['weeks']-=4;
            $tiempo_experiencia['months']+=1;
        }
        if($tiempo_experiencia['months']>=12){
            $tiempo_experiencia['months']-=12;
            $tiempo_experiencia['years']+=1;
        }
        if($tiempo_experiencia['years']>0){
            return $tiempo_experiencia['years']." años ";
        }else if($tiempo_experiencia['months']>0){
            return $tiempo_experiencia['months']." meses";
        }else{
            return null;
        }
    }
}
