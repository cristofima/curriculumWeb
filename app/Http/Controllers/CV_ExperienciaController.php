<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class CV_ExperienciaController extends Controller{
    
    public function index(){
        $experiencia=Auth::user()->experiencie()->orderBy('experiencia_fin','DESC')->get();
        return view('panel.job_experience.index',compact('experiencia'));
    }

}
