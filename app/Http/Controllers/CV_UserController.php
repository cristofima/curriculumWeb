<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\CV_ProfileRequest;
use App\Http\Requests\CV_PasswordRequest;

class CV_UserController extends Controller{
    
    public function account(){
        $usuario=Auth::user();
        return view('panel.account',compact('usuario'));
    }

    public function update_profile(CV_ProfileRequest $request){
        $usuario=Auth::user();
        $usuario->name=$request->name;
        $usuario->email=$request->email;
        $usuario->update();
        return Redirect::to('account')->with('success', 'Perfil actualizado');
    }

    public function update_password(CV_PasswordRequest $request){
        $usuario=Auth::user();
        $usuario->password=bcrypt($request->new_password);
        $usuario->update();
        return Redirect::to('account')->with('success', 'Contraseña actualizada');
    }
}
