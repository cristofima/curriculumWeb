<?php

namespace App\Http\Controllers;

use Auth;
use App\CV_Ponencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CV_PonenciaController extends Controller{
    
    public function index(){
        $ponencias=Auth::user()->presentations()->orderBy('ponencia_fecha','DESC')->get();
        return view('panel.presentations.index',compact('ponencias'));
    }
}
