<?php

namespace App\Http\Controllers;

use Auth;
use App\CV_Certificacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
class CV_CertificacionController extends Controller
{
    
    public function index(){
        $certificaciones=Auth::user()->certifications()->orderBy('certificacion_fecha','DESC')->get();
        return view('panel.certifications.index',compact('certificaciones'));
    }
}
