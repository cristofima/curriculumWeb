<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GEN_Provincia;
use App\CV_Estudio;
use Auth;
use App\Http\Requests\CV_EstudioRequest;
use Illuminate\Support\Facades\Redirect;

class CV_EstudioController extends Controller{
    
    public function index(){
        $estudios=Auth::user()->studies()->orderBy('estudio_inicio','DESC')->paginate(10);
        $provincias=GEN_Provincia::orderBy('provincia_id')->get();
        return view('panel.studies.index', compact('estudios','provincias'));
    }

    public function store(CV_EstudioRequest $request){
        CV_Estudio::create($request->except(['_token','estudio_nacional']));
        return Redirect::to('studies')->with('success', 'Estudio guardado');
    }
}
