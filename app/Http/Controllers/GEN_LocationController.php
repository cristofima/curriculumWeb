<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GEN_Provincia;

class GEN_LocationController extends Controller{
    
    public function getCantons(Request $request){
        $idProvincia = trim($request->idProvincia);
        $cantones = GEN_Provincia::findOrFail($idProvincia)->cantons;
        return response()->json($cantones);
    }
}
