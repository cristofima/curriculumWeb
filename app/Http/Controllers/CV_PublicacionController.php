<?php

namespace App\Http\Controllers;

use Auth;
use App\CV_Publicacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CV_PublicacionController extends Controller{

    public function index(){
        $publicaciones=Auth::user()->publications()->orderBy('publicacion_fecha','DESC')->get();
        return view('panel.publications.index',compact('publicaciones'));
    }
}
