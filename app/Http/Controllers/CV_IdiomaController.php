<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\CV_Idioma;
use App\GEN_Idioma;
use App\Http\Requests\CV_IdiomaRequest;

class CV_IdiomaController extends Controller{
    
    public function index(){
        $idiomasExt=Auth::user()->languajes()->paginate(10);
        $idiomas=GEN_Idioma::orderBy('idioma_nombre')->get();
        return view('panel.languajes.index', compact('idiomasExt','idiomas'));
    }

    public function store(CV_IdiomaRequest $request){
        CV_Idioma::create($request->except(['_token']));
        return Redirect::to('languajes')->with('success', 'Idioma guardado');
    }
}
