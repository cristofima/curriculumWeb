<?php

namespace App\Http\Controllers;

use Auth;

class PDFController extends Controller
{

    public function index()
    {
        $estudios = Auth::user()->studies()->orderBy('estudio_inicio', 'DESC')->get();
        $cursos = Auth::user()->capacitations()->where('capacitacion_tipo', 'CU')->orderBy('capacitacion_fin', 'DESC')->get();
        $talleres = Auth::user()->capacitations()->where('capacitacion_tipo', 'TA')->orderBy('capacitacion_fin', 'DESC')->get();
        $conferencias = Auth::user()->capacitations()->whereIn('capacitacion_tipo', ['JO', 'CO', 'SE'])->orderBy('capacitacion_fin', 'DESC')->get();
        $ponencias = Auth::user()->presentations()->orderBy('ponencia_fecha', 'DESC')->get();
        $personalData = Auth::user()->personalData;
        $pdf = \PDF::loadView('panel.reports.curriculum', compact("personalData", "estudios", "cursos", "talleres", "conferencias", "ponencias"));
        return $pdf->stream('Curriculum Vitae.pdf');
    }
}
